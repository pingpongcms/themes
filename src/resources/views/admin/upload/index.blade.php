@extends('dashboard::layouts.master')

@section('content')
	
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
            <div class="col-md-11">
                <h3>Upload Theme</h3>
            </div>
            <div class="col-md-1">
                <a href="{!! route('admin.themes.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
	</div>
	<div class="panel-body">
		{!! Form::open(['files' => true]) !!}
			<div class="form-group">
				{!! Form::file('file') !!}
				<p class="help-block">
					Supported format: ZIP.
				</p>
				{!! error_for('file', $errors) !!}
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Upload</button>
			</div>
		{!! Form::close() !!}
	</div>
</div>

@stop