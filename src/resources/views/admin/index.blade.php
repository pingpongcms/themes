@extends('dashboard::layouts.master')

@section('content')

  <div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-11">
                <h3>Themes ({{ $themes->count() }})</h3>
            </div>
            <div class="col-md-1">
                <a href="{!! route('admin.themes.upload.index') !!}" class="btn btn-default">Upload</a>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th width="5%" class="text-center">#</th>
            <th>Name</th>
            <th>Description</th>
            <th>Activated</th>
            <th class="text-center">Action</th>
        </tr>
        @foreach ($themes as $index => $theme)
            <tr>
                <td class="text-center">{{ $index + 1 }}</td>
                <td>{{ $theme->getName() }}</td>
                <td>{{ $theme->getDescription() }}</td>
                <td>{{ $theme->activated() ? 'Yes' : 'No' }}</td>
                <td class="text-center">
                    <div class="btn-group">
                        @unless($theme->activated())
                            {!! Form::open(['route' => 'admin.settings.update']) !!}
                                {!! Form::hidden('settings[theme_current]', $theme->getName()) !!}
                                <button class="btn btn-sm btn-default">Activate</button>
                            {!! Form::close() !!}
                        @endunless
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>

@stop