<?php

namespace Pingpongcms\Themes\Providers;

use Illuminate\Support\ServiceProvider;
use Pingpongcms\Themes\Console\ListCommand;
use Pingpongcms\Themes\Console\PublishCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    public function register()
    {
    	$this->commands(ListCommand::class);
    	$this->commands(PublishCommand::class);
    }
}