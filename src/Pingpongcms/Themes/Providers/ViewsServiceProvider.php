<?php

namespace Pingpongcms\Themes\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\FileViewFinder;

class ViewsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->bind('view.finder', function ($app) {
            $theme = $this->app['themes']->current();

            $defaultViewPaths = $app['config']['view.paths'];

            if (is_dir($path = $theme->getPath('views'))) {
                $paths = array_merge([$path], $defaultViewPaths);
            } else {
                $paths = $defaultViewPaths;
            }

            return new FileViewFinder($app['files'], $paths);
        });
    }
}
