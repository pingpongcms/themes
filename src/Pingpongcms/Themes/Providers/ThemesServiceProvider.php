<?php

namespace Pingpongcms\Themes\Providers;

use Illuminate\Support\ServiceProvider;
use Pingpongcms\Terms\Http\Composers\ShareParents;
use Pingpongcms\Themes\Repository;

class ThemesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../../resources/lang', 'themes');

        $this->loadViewsFrom(realpath(__DIR__.'/../../../resources/views'), 'themes');

        $configPath = __DIR__.'/../../../config/themes.php';

        $this->publishes([
            $configPath => config_path('themes.php'),
        ]);

        $this->mergeConfigFrom(
            $configPath, 'themes'
        );
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('themes', function ($app)
        {
            $path = config('themes.path', base_path('resources/views'));

            $themes = new Repository($path, $app['files']);
            
            $themes->setCurrent(setting('theme_current', config('themes.current')));

            return $themes;
        });
    }
}
