<?php

namespace Pingpongcms\Themes\Console;

use Illuminate\Console\Command;

class ListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all available themes';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $themes = [];

        foreach ($this->laravel['themes']->all()->remap() as $theme) {
            $themes[] = [
                $theme->getName(),
                $theme->getDescription(),
                $theme->getPath(),
                $theme->activated() ? 'Yes': 'No'
            ];
        }

        $this->table(['Name', 'Description', 'Path', 'Activated?'], $themes);
    }
}
