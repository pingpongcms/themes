<?php

namespace Pingpongcms\Themes\Console;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:publish {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish theme assets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        if ($name = $this->argument('name')) {
            $this->publish($name);
        } else {
            $this->publishAll();
        }
    }

    /**
     * Publish theme assets by given theme name.
     * 
     * @param  string $name
     * @return void
     */
    protected function publish($name)
    {
        $theme = $this->laravel['themes']->find($name = strtolower($name));
        
        $sourcePath = $theme->getPath('assets');

        $destinationPath = base_path("public/themes/{$name}");

        $this->laravel['files']->copyDirectory($sourcePath, $destinationPath);

        $this->info("Theme [{$name}] published!");
    }

    /**
     * Publish all theme assets.
     * 
     * @return void
     */
    protected function publishAll()
    {
        foreach ($this->laravel['themes']->all() as $theme) {
            $this->publish($theme['name']);
        }
    }
}
