<?php

namespace Pingpongcms\Themes;

use Illuminate\Support\Collection as BaseCollection;

class Theme extends BaseCollection
{
    /**
     * Get theme name.
     * 
     * @return string|null
     */
    public function getName()
    {
        return $this->get('name');
    }

    /**
     * Get theme description.
     * 
     * @return string|null
     */
    public function getDescription()
    {
        return $this->get('description');
    }

    /**
     * Get authors.
     * 
     * @return mixed
     */
    public function getAuthors()
    {
        return $this->get('authors');
    }

    /**
     * Determine whether this theme is the current theme.
     * 
     * @return bool
     */
    public function activated()
    {
        return Facades\Theme::getCurrent() == $this->getName();
    }

    /**
     * Get theme path.
     *
     * @param string|null $extra
     * @return string
     */
    public function getPath($extra = null)
    {
        return $this->get('path').($extra ? '/'.$extra : '');
    }
}