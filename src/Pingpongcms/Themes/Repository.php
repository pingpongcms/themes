<?php

namespace Pingpongcms\Themes;

use Illuminate\Filesystem\Filesystem;
use Pingpong\Support\Json;

class Repository
{
    /**
     * The themes path.
     * 
     * @var string
     */
    protected $path;

    /**
     * The Filesystem instance.
     * 
     * @var Filesystem
     */
    protected $files;

    /**
     * The current theme.
     * 
     * @var string
     */
    protected $current;

    /**
     * Determine whether the themes has have scanned or not.
     * 
     * @var boolean
     */
    protected $scanned = false;

    /**
     * Create new instance of this class.
     * 
     * @param string|null $path
     * @param Filesystem|null $files
     */
    public function __construct($path = null, Filesystem $files = null)
    {
        $this->path = $path;
        $this->files = $files ?: new Filesystem;
        $this->collection = new Collection;
    }

    /**
     * Gets the value of path.
     *
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets the value of path.
     *
     * @param mixed $path the path
     *
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Gets the value of files.
     *
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Sets the value of files.
     *
     * @param Filesystem $files the files
     *
     * @return self
     */
    public function setFiles(Filesystem $files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get all available themes.
     * 
     * @return Collection
     */
    public function all()
    {
        $files = $this->files->glob($this->path.'/**/theme.json');

        if (! $this->scanned) {
            foreach ($files as $file) {
                $theme = array_add(Json::make($file)->toArray(), 'path', dirname($file));

                $this->collection->push($theme);
            }

            $this->scanned = true;
        }

        return $this->collection;
    }

    /**
     * Get the current theme.
     * 
     * @return Theme
     */
    public function current()
    {
        return $this->all()->find($this->current);
    }

    /**
     * Find the theme.
     *
     * @param string $name
     * @return Theme
     */
    public function find($name)
    {
        return $this->all()->find($name);
    }

    /**
     * Generate theme asset url.
     * 
     * @param  string $url
     * @return string
     */
    public function asset($url)
    {
        return asset("themes/{$this->current}/{$url}");
    }

    /**
     * Gets the value of current.
     *
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Sets the value of current.
     *
     * @param mixed $current the current
     *
     * @return self
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }
}