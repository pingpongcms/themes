<?php

namespace Pingpongcms\Themes;

use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{
    /**
     * Find a theme by given theme name.
     * 
     * @param  string $name
     * @return Theme
     */
    public function find($name)
    {
        return new Theme($this->where('name', $name)->first());
    }

    /**
     * Maps every theme array to a new instance of Theme.
     * 
     * @return self
     */
    public function remap()
    {
        return $this->map(function ($item)
        {
            return new Theme($item);
        });
    }
}