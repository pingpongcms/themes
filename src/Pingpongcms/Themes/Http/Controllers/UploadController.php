<?php

namespace Pingpongcms\Themes\Http\Controllers;

use App\Http\Controllers\Controller;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Support\Facades\File;
use Pingpongcms\Themes\Facades\Theme;
use Pingpongcms\Themes\Http\Requests\UploadRequest;

class UploadController extends Controller
{
	public function __construct()
	{
        // $this->authorize('upload_theme');	
	}

    public function index()
    {
    	return view('themes::admin.upload.index');
    }

    public function store(UploadRequest $request)
    {
        $zipper = Zipper::make($file = $request->file('file'));

        $folderName = head(explode('.', $file->getClientOriginalName()));

        $manifest = collect(json_decode($zipper->getFileContent($folderName.'/theme.json'), true));
        
        $name = $manifest->get('name');

        $destinationFolder = config('themes.path').'/'.$name;

        if (! is_dir($destinationFolder)) {
            mkdir($destinationFolder, 0755, true);
        }

        $zipper->folder($folderName)->extractTo($destinationFolder);

        $theme = Theme::find($name);

        if (! is_dir($assetsFolder = public_path("themes/{$name}"))) {
            mkdir($assetsFolder, 0755, true);
        }

        if (is_dir($path = $theme->getPath('assets'))) {
            File::copyDirectory($path, $assetsFolder);
        }

        event('theme.uploaded', $theme);

    	return redirect()->route('admin.themes.index');
    }
}