<?php

namespace Pingpongcms\Themes\Http\Controllers;

use App\Http\Controllers\Controller;
use Pingpongcms\Themes\Facades\Theme;

class ThemesController extends Controller
{
    public function index()
    {
        $this->authorize('update-theme_current');
        
        $themes = Theme::all()->remap();

        return view('themes::admin.index', compact('themes'));
    }
}