<?php

Route::group(['prefix' => 'themes'], function ()
{
	Route::get('/', [
	    'as' => 'themes.index',
	    'uses' => 'ThemesController@index'
	]);

	Route::group(['prefix' => 'upload'], function ()
	{
		Route::get('/', [
		    'as' => 'themes.upload.index',
		    'uses' => 'UploadController@index'
		]);
		Route::post('/', [
		    'as' => 'themes.upload.store',
		    'uses' => 'UploadController@store'
		]);
	});
});