<?php

namespace Pingpongcms\Themes\Facades;

use Illuminate\Support\Facades\Facade;

class Theme extends Facade
{
    /**
     * Get the name of component.
     * 
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'themes';
    }
}